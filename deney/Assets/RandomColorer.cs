﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorer : MonoBehaviour
{
    Color randomColor;
    // Start is called before the first frame update
    void Start()
    {
        randomColor = new Color
        (
        Random.Range(0f, 1f),
        Random.Range(0f, 1f),
        Random.Range(0f, 1f),
        Random.Range(0f, 1f)
        );
        GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
