﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixRotation : MonoBehaviour
{
    Quaternion rotation;
    void Awake()
    {
        rotation = Quaternion.Euler(90, 0, 0);
    }
    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
