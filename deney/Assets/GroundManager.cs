﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
    public GameObject[] groundPrefabs;
    private Transform playerTransform;
    private float spawnZ = 0.0f;
    private float groundLenght = 100.0f;
    private int amnGroundOnScreen;
    private List<GameObject> activateGrounds;
    public GameObject endGround;
    private GameManager gm;
    private Color randomColor;

    void Start()
    {
        randomColor = new Color(
      Random.Range(0f, 1f),
      Random.Range(0f, 1f),
      Random.Range(0f, 1f),
      Random.Range(0f, 1f)
  );
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        amnGroundOnScreen = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().amnGroundOnScreen;
        activateGrounds = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < amnGroundOnScreen; i++)
        {
            SpawnGround();
        }
        SpawnEndGround();
    }
    private void SpawnGround(int prefabIndex = -1)
    {
        GameObject go;
        GameObject selected;
        
        
        
        if (gm.level <= 10)
        {
            selected = groundPrefabs[Random.Range(0, 10)];
            //selected.GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
        }
        else if (gm.level > 10 && gm.level < 20)
        {
        selected = groundPrefabs[Random.Range(0, 15)];
        //selected.GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
        }
        else if (gm.level > 20 && gm.level < 30)
        {
        selected = groundPrefabs[Random.Range(0, 20)];
        //selected.GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
        }
        else if (gm.level > 30 && gm.level < 40)
        {
        selected = groundPrefabs[Random.Range(0, 25)];
        //selected.GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
        }
        else
        {
            selected = groundPrefabs[Random.Range(0, groundPrefabs.Length)];
            //selected.GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
        }
        if (activateGrounds.Count == 0)
        {
            go = Instantiate(selected) as GameObject;
            go.transform.SetParent(transform);
            go.transform.position = Vector3.forward * spawnZ;
            spawnZ += groundLenght;
            activateGrounds.Add(selected);
        }
        else if (selected != activateGrounds[activateGrounds.Count - 1])
        {
            go = Instantiate(selected) as GameObject;
            go.transform.SetParent(transform);
            go.transform.position = Vector3.forward * spawnZ;
            spawnZ += groundLenght;
            activateGrounds.Add(selected);
        }
        else
        {
            SpawnGround();
        }

    }

    private void SpawnEndGround()
    {
        GameObject end;
        end = Instantiate(endGround) as GameObject;
        end.transform.SetParent(transform);
        end.transform.position = Vector3.forward * spawnZ;
        spawnZ += groundLenght;
        activateGrounds.Add(end);
    }
}
