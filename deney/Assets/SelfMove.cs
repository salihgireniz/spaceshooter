﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfMove : MonoBehaviour
{
    public GameObject[] selfMove;
    int current1 = 0;
    float rotSpeed;
    public float selfSpeed;
    float WPradius = 1;

    
    void Update()
    {
        
        if(Vector3.Distance(selfMove[current1].transform.position,transform.position) < WPradius)
        {
            current1++;
            if(current1 >= selfMove.Length)
            {
                current1 = 0;
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, selfMove[current1].transform.position, Time.deltaTime * selfSpeed);
    }
}
