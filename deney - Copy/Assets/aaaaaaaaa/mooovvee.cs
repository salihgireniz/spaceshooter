﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mooovvee : MonoBehaviour
{
    Rigidbody db;
    void Start()
    {
        db = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        db.velocity = -transform.forward*100f;
    }
}
