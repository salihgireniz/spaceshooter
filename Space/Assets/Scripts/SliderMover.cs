﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMover : MonoBehaviour
{
    public Text lvlText;
    public Text fasterText;
    public Slider slider;
    public int lvl = 1;
    public GameObject speedLower;
    //public GameObject BGSpeed;
    public GameObject astroidSpeed;
    public GameObject astroidSpeed1;
    public GameObject astroidSpeed2;

    public static SliderMover instance;
    public int sliderValueLower = 100;
    //public float target;



    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        slider = FindObjectOfType<Slider>();
        slider.value = 0;
        
    }
    void FixedUpdate()
    {
        if (slider.value == 1)
        {
            StartCoroutine(faster());
            slider.value = 0;
            lvl += 1;
            lvlText.text = ("LEVEL " + lvl);
            if (sliderValueLower >= 50)
            { 
                sliderValueLower = sliderValueLower * (100 / 107);
            }
            if (speedLower.GetComponent<GameManager>().spawnWait > 0.3f)
            {
                speedLower.GetComponent<GameManager>().spawnWait = speedLower.GetComponent<GameManager>().spawnWait - 0.5f;
                astroidSpeed.GetComponent<LaserMover>().speed = astroidSpeed.GetComponent<LaserMover>().speed - 5f;
                astroidSpeed1.GetComponent<LaserMover>().speed = astroidSpeed.GetComponent<LaserMover>().speed - 5f;
                astroidSpeed2.GetComponent<LaserMover>().speed = astroidSpeed.GetComponent<LaserMover>().speed - 5f;
                speedLower.GetComponent<GameManager>().hazardCount = speedLower.GetComponent<GameManager>().hazardCount + 2;
                astroidSpeed.GetComponent<LaserMover>().speed = astroidSpeed.GetComponent<LaserMover>().speed - 5f;
                speedLower.GetComponent<SymbolSpawner>().spawningTime = speedLower.GetComponent<SymbolSpawner>().spawningTime - 2.5f;
                //target = BGSpeed.GetComponent<BackGround>().scrollSpeedx + 1f;
            }
        }


        //if ( BGSpeed.GetComponent<BackGround>().scrollSpeedx<target)
        {
            //BGSpeed.GetComponent<BackGround>().scrollSpeedx = BGSpeed.GetComponent<BackGround>().scrollSpeedx + 0.0008f;
        }
    }
    public void valueUp(float sliderValue)
    {
        slider.value = slider.value + sliderValue;
    }
    
    IEnumerator faster()
    {
        fasterText.text = ("SPEED UP!");
        yield return new WaitForSeconds(2);
        fasterText.text = ("");
        
    }
    public IEnumerator PowerUp()
    {
        fasterText.text = ("POWER UP!");
        yield return new WaitForSeconds(5);
        fasterText.text = ("");

    }
}
