﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotSpawner : MonoBehaviour
{

    private Rigidbody rb;



    public GameObject shot;
    public GameObject shotSpawn;
    public float fireRate;
    private float nextFire;



    void Update()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.transform.position, shotSpawn.transform.rotation);
            GetComponent<AudioSource>().Play();
        }

    }
    void Start()
    {
        
        rb = GetComponent<Rigidbody>();
    }
}
