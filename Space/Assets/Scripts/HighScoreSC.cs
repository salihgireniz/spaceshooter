﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HighScoreSC : MonoBehaviour
{
    public Text highScore;
    

    void Start()
    {
        highScore.text ="HIGH SCORE "+ PlayerPrefs.GetFloat("BestScore", 0).ToString();
    }

    
}
