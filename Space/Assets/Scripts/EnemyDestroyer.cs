﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyer : MonoBehaviour
{
    private float i = 1;
    public float shootNum = 3;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "EnemyLaser" || other.tag == "Astroid" || other.tag == "Enemy")
        {
            return;
        }
        if (i < shootNum && other.tag == "Laser")
        {
            Destroy(other.gameObject);
            i++;
            return;
        }
        
    }
}
