﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SymbolSpawner : MonoBehaviour
{
    public GameObject[] symbols;
    public Vector3 spawnValues;
    public float spawningTime;

    void Start()
    {
        StartCoroutine(CreateSymbol());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator CreateSymbol()
    {
        yield return new WaitForSeconds(spawningTime);
        while (true)
        {
            GameObject symbol = symbols[Random.Range(0, symbols.Length)];
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(symbol, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(spawningTime);
        }
        
    }
}
