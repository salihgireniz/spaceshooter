﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    public GameObject enemyExplosion;
    public GameObject enemySound;
    public int scoreValue;
    public float sliderValue;

    private float i = 1;
    public float shootNum = 3;
    public GameObject miniExplosion;

    void Start()
    {
        enemySound = GameObject.Find("EnemyAudioKeeper");
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
    }


    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "symbol"||other.tag == "symbol1" || other.tag == "symbol2" || other.tag == "Astroid" || other.tag == "EnemyLaser")
        {
            return;
        }
        else if (other.tag == ("Boundary"))
        {
            Destroy(gameObject);
        }
        else if (i < shootNum && other.tag == "Laser")
        {
            Instantiate(miniExplosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            i++;
            return;
        }
        else
        {
            Destroy(gameObject);
            Instantiate(enemyExplosion, gameObject.transform.position, gameObject.transform.rotation);
            enemySound.GetComponent<AudioSource>().Play();
            GameManager.instance.AddScore(scoreValue);
            SliderMover.instance.valueUp(sliderValue);
        }
    }
}

