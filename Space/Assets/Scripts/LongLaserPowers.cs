﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongLaserPowers : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            return;
        }
        if(other.tag == "Laser" || other.tag == "EnemyLaser")
        {
            Destroy(other.gameObject);
        }
        Destroy(other.gameObject);
    }
}
