﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBorders : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            player.transform.position = new Vector3(
            Mathf.Clamp(player.transform.position.x, -90, 90), 0f, Mathf.Clamp(player.transform.position.z, -5, 150));
        }
        
    }
}
