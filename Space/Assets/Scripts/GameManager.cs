﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    private int score;
    public static GameManager instance; //Easy way..

    public Vector3 replayPosition = new Vector3(0, 0, 0);
    public Quaternion replayRotation = Quaternion.Euler(0,0,0);
    public GameObject replayButton;
    public GameObject gameInHighScore;

    public void Awake()
    {
        instance = this;
    } //Easy way..
    void Start()
    {
        score = 0;
        UpdateScore();
        StartCoroutine (spawnWaves());
    }
    IEnumerator spawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = "SCORE:" + score;
        float currentBestScore = PlayerPrefs.GetFloat("BestScore", 0);
        float currentScore = score;
        if (currentScore > currentBestScore)
        {
            PlayerPrefs.SetFloat("BestScore", currentScore);
        }
    }
    public void Restart()
    {
        StartCoroutine ("Restart2");
    }
    IEnumerator Restart2()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject CanvasObject = GameObject.Find("Canvas");

        GameObject replay = Instantiate(replayButton, replayPosition, replayRotation);
        replay.transform.SetParent(CanvasObject.transform);
        replay.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

        GameObject InGameHighScore = Instantiate(gameInHighScore, replayPosition + new Vector3(0, 20, 0), replayRotation);
        InGameHighScore.transform.SetParent(CanvasObject.transform);
        InGameHighScore.GetComponent<RectTransform>().localPosition = new Vector3(0, 300, 0);
    }
    
}
