﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DestroyByContact : MonoBehaviour
{
    
    public float shootNum = 3;
    public GameObject explosion;
    public GameObject playerExplosion;
    private float i=1;
    public int scoreValue;
    public static DestroyByContact instance;
    public float sliderValue;

    public GameObject miniExplosion;

    void Awake()
    {
        instance = this;
        
    }
    
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy" || other.tag =="Boundary")
        {
            return;
        }
        if(other.tag == "powershield"|| other.tag == "LongLaser")
        {
            Instantiate(explosion, transform.position, transform.rotation);
            GameManager.instance.AddScore(scoreValue);
            SliderMover.instance.valueUp(sliderValue);            
            Destroy(gameObject);
            return;
        }
        if (i < shootNum && other.tag=="Laser")
        {
            Instantiate(miniExplosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            i++;
            return;   
        }

        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player")
        {
            
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            GameManager.instance.Restart();
            
        }
        GameManager.instance.AddScore(scoreValue); 
        SliderMover.instance.valueUp(sliderValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
    
}
