﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireForSpawns : MonoBehaviour
{
    public float nextFire;
    public float fireRate;
    public GameObject shot;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, transform.position, transform.rotation);           
            
        }
    }
}
