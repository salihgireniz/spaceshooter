﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUnbreak : MonoBehaviour
{
    public GameObject miniExplosion;
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            //other.GetComponent<MeshCollider>().enabled = false;
            return;
        }
        else if(other.tag == "Laser")
        {
            return;
        }
        else
        {
            Instantiate(miniExplosion, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(other.gameObject);
        }
    }
}
