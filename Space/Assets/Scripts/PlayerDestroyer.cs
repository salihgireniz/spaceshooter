﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class PlayerDestroyer : MonoBehaviour
{
    public GameObject playerExplosion;
    public Collider player;
    public GameObject playerAudioKeeper;
    public GameObject laserSpawnPosition;
    public GameObject newlaserSpawnPosition;
    public GameObject newlaserSpawnPosition2;
    public GameObject powerShield;
    public GameObject laserShot;
    public GameObject miniExplosion;



    void Start()
    {
        
            }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Laser" || other.tag == "LongLaser") 
        {
            return;
        }
        else if(other.tag == "symbol")
        {
            StartCoroutine(SliderMover.instance.PowerUp());
            Destroy(other.gameObject);
            StartCoroutine(symbolEffect());
            return;
        }
        else if (other.tag == "symbol1")
        {
            StartCoroutine(SliderMover.instance.PowerUp());
            Destroy(other.gameObject);
            StartCoroutine(symbol1Effect());
            return;
        }
        else if(other.tag == ("powershield"))
        {
            return;
        }
        else if(other.tag == "symbol2")
        {
            StartCoroutine(SliderMover.instance.PowerUp());
            Destroy(other.gameObject);
            StartCoroutine(symbol2Effect());
            return;
        }
        else
        {
            Instantiate(miniExplosion, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(gameObject);
            playerAudioKeeper.GetComponent<AudioSource>().Play();
            Instantiate(playerExplosion, gameObject.transform.position, gameObject.transform.rotation);
            CameraShaker.Instance.ShakeOnce(6f, 6f, 0.2f, 2f);
            GameManager.instance.Restart();
        }
    }

    IEnumerator symbolEffect()
    {
        
        GameObject SpawnClone1 = Instantiate(newlaserSpawnPosition, laserSpawnPosition.transform.position + new Vector3(-10, 0, 0), laserSpawnPosition.transform.rotation)as GameObject;
        SpawnClone1.transform.parent = gameObject.transform;


        GameObject SpawnClone2 = Instantiate(newlaserSpawnPosition2, laserSpawnPosition.transform.position + new Vector3(10, 0, 0), laserSpawnPosition.transform.rotation)as GameObject;
        SpawnClone2.transform.parent = gameObject.transform;

        yield return new WaitForSeconds(5);
        Destroy(SpawnClone1);
        Destroy(SpawnClone2);
    }
    IEnumerator symbol1Effect()
    {
        GameObject ShieldObject = Instantiate(powerShield, gameObject.transform.position, Quaternion.Euler(0,-90,0)) as GameObject;
        ShieldObject.transform.parent = gameObject.transform;
        yield return new WaitForSeconds(5);
        Destroy(ShieldObject);
    }
    IEnumerator symbol2Effect()
    {
        gameObject.GetComponent<PlayerMove>().enabled = false;
        GameObject LaserShot = Instantiate(laserShot, laserSpawnPosition.transform.position + new Vector3(0,0,180), laserSpawnPosition.transform.rotation) as GameObject;
        LaserShot.transform.parent = gameObject.transform;
        yield return new WaitForSeconds(5);
        Destroy(LaserShot);
        gameObject.GetComponent<PlayerMove>().enabled = true;
    }
}