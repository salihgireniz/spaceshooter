﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ontrigger : MonoBehaviour
{
    public Transform roadPrefab;
    // Start is called before the first frame update
    void OnTriggerEnter (Collider yayother)
    {
        Instantiate(roadPrefab, new Vector3(0, 0, transform.parent.position.z+1000f), roadPrefab.rotation);
    }
}
