﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround : MonoBehaviour
{
    public float scrollSpeed = 0.05F; // speed
    public float scrollSpeedx = 1F; // speed
    public float offset=0;

    public new Renderer renderer;
    void Start()
    {
        scrollSpeed = 0.05f;
        scrollSpeedx = 1;
        offset = 0;
        renderer.material.mainTextureOffset = new Vector2(0, 0);
        renderer = GetComponent<Renderer>();
    }

    void Update()
    {
      
        //smooth
        offset =  Time.timeSinceLevelLoad* scrollSpeed * scrollSpeedx;

        //texture offset
        renderer.material.mainTextureOffset = new Vector2(0, offset);
    }
}
