﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movveee : MonoBehaviour
{
    Rigidbody rb;
    float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = 10f;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = new Vector3(0f, 0f, 1f) * speed;
    }
}
