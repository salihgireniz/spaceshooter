using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RandomObstacle : MonoBehaviour
{
    public GameObject obstacle;
    public int obstacleCount;


    public float zMin;
    public float zMax;
    public float xMin;
    public float xMax;
    public float y=3f;

    private Color randomColor = Color.yellow;
    
    void Awake()
    {

    }

    void obstacleRandomizer()
    {
        Vector3 spawnPosition1 = new Vector3(Random.Range(xMin, xMax), y, Random.Range(zMin, zMax));
        Quaternion spawnRotation1 = Quaternion.Euler(0, 0, 0);

        Vector3 spawnPosition2 = new Vector3(Random.Range(xMin, xMax), y, Random.Range(zMin, zMax));
        Quaternion spawnRotation2 = Quaternion.Euler(0, 0, 0);

        Vector3 spawnPosition3 = new Vector3(Random.Range(xMin, xMax), y, Random.Range(zMin, zMax));
        Quaternion spawnRotation3 = Quaternion.Euler(0, 0, 0);

        if (spawnPosition2.z-spawnPosition1.z<5.1f || spawnPosition1.z - spawnPosition2.z <5.1f)
        {
            if(spawnPosition1.z < spawnPosition2.z)
            {
                spawnPosition2.z = spawnPosition2.z + 5.1f;
            }
            else
            {
                spawnPosition1.z = spawnPosition1.z + 5.1f;
            }
        }
        GameObject obstacleGO=Instantiate(obstacle, spawnPosition1, spawnRotation1)as GameObject;
        obstacleGO.GetComponent<MeshRenderer>().material.color = randomColor;

        GameObject obstacleGO2 = Instantiate(obstacle, spawnPosition2, spawnRotation2)as GameObject;
        obstacleGO2.GetComponent<MeshRenderer>().material.color = randomColor;

        if ((spawnPosition3.z - spawnPosition1.z > 5.1f || spawnPosition1.z - spawnPosition3.z > 5.1f) && (spawnPosition3.z - spawnPosition2.z > 5.1f || spawnPosition2.z - spawnPosition3.z > 5.1f))
        {
            GameObject obstacleGO3 = Instantiate(obstacle, spawnPosition3, spawnRotation3)as GameObject;
            obstacleGO3.GetComponent<MeshRenderer>().material.color = randomColor;

        }


    }
    void Start()
    {
        
        obstacleRandomizer();
       // obstacle.GetComponent<MeshRenderer>().material.color = randomColor;
    }


}
