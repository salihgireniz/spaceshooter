﻿using UnityEngine;
using System.Collections;

public class TouchScript : MonoBehaviour
{


    private Vector3 targetPos;
    public float xIncrement;
    public float speed;
    void FixedUpdate()
    {
        

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            
            targetPos = new Vector3(transform.position.x , transform.position.y + xIncrement, transform.position.z);
            transform.position = targetPos;
        }

        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            
            targetPos = new Vector3(transform.position.x , transform.position.y - xIncrement, transform.position.z);
            transform.position = targetPos;
        }
    }
}
